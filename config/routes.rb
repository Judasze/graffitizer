Graffitizer::Application.routes.draw do

  # This line mounts Forem's routes at /forums by default.
  # This means, any requests to the /forums URL of your application will go to Forem::ForumsController#index.
  # If you would like to change where this extension is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Forem relies on it being the default of "forem"
  mount Forem::Engine, :at => '/forums'


  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # This line mounts Forem's routes at /forums by default.
  # This means, any requests to the /forums URL of your application will go to Forem::ForumsController#index.
  # If you would like to change where this extension is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Forem relies on it being the default of "forem"
  devise_for :users

  get "home/index"
  root to: 'home#index'

  resources :throwups, only: [:new, :create, :show]
  resources :messages, only: [:new, :create, :index, :show]
  resources :chats, only: [:index, :create]
  resource :user, as: :present_user, controller: 'present_user', only: [:show, :edit, :update]

  resources :throwups, only: [:new, :create, :index, :show] do
    resources :comments, only: [:new, :create, :update, :show]
    resource :like, only: [:create, :destroy]
  end

  resources :users, only: [:show, :index] do
    resources :throwups, module: 'users', only: [:index]
  end

  resources :battle_entries do
    resource :battle_vote, only: [:create, :destroy]
  end

  resources :battles, only: [:new, :create, :index, :show] do
    resources :battle_entries, only: [:new, :create, :index, :show]
  end
end
