# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Graffitizer::Application.config.secret_key_base = 'aff3cd2f9675459a0b51b62ef9f4beadd2bf251cd349ada0654c6e4c038722c4f797dd6b394e4d23cbe2d2884db20b47044db19a667caa9b47bfaeb79516260b'
