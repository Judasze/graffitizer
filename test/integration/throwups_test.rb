require 'test_helper'

class ThrowupsTest < ActionDispatch::IntegrationTest

  def setup
    @user = FactoryGirl.create(:user)
    login(@user)
  end


  test 'log user in' do
    assert_select 'body', Regexp.new(@user.name)
  end

  test 'create users throwup' do
    counter = @user.throwups.count
    assert_difference 'Throwup.count' do
      throwup('test', 'www.youtube.com')
    end
    assert_equal counter + 1, @user.throwups.count
  end

  test 'user comments throwup' do
    @throwup = FactoryGirl.create(:throwup, user: @user)
    comment("pięset", @throwup)
    c = Comment.last
    assert_equal @throwup.user, @user
    assert_equal @user.comments.last, c
    assert_equal @throwup.comments.last, c
  end

  private

  def login(user)
    post_via_redirect "/users/sign_in", user: { email: user.email, password: user.password }
    assert_equal root_path, path
  end

  def throwup(content, youtube)
    post_via_redirect "/throwups", throwup: { content: content, youtube: youtube }
    assert_equal throwups_path, path
  end

  def comment(content, throwup)
    post_via_redirect throwup_comments_path(throwup_id: throwup.id), comment: { content: content }
    assert_equal throwups_path, path
  end
end
