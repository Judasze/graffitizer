module Attachments
  extend ActionDispatch::TestProcess
end

FactoryGirl.define do
  factory :throwup do
    content "malkontent"
    youtube "www.youtube.ru"
    user
    attachment { Attachments.fixture_file_upload(Rails.root.join("test/fixtures/soldier.jpg"), "image/jpg") }
  end

  sequence :email do |n|
    "john@#{n}rambo.com"
  end

  sequence :name do |n|
    "johnrambo#{n}"
  end

  factory :user, :aliases => [:sender, :recipient] do
    name
    email
    password 'password'
  end

  factory :message do
    subject "Subject"
    content "Concent"
    sender
    recipient
  end
end
