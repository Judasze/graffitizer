require 'test_helper'

class ThrowupTest < ActiveSupport::TestCase
  test 'throwup should not be saved without attachment or youtube' do
    @throwup = FactoryGirl.build(:throwup, user: @user, youtube: nil, attachment: nil)
    assert(!@throwup.valid?)
    assert(@throwup.errors[:attachment].any?)
    assert(@throwup.errors[:youtube].any?)
  end

  test 'throwup with attachment but without youtube' do
    @throwup = FactoryGirl.build(:throwup, user: @user, youtube: nil)
    assert(@throwup.valid?)
    assert(!@throwup.errors[:youtube].any?)
  end

  test '#attachment_nil?' do
    assert !FactoryGirl.build(:throwup, attachment: attachment).attachment_nil?
  end

  def attachment
    Attachments.fixture_file_upload(Rails.root.join("test/fixtures/soldier.jpg"), "image/jpg")
  end

  test 'throwup with youtube but without attachment' do
    @throwup = FactoryGirl.build(:throwup, user: @user, attachment: nil)
    assert(@throwup.valid?)
    assert(!@throwup.errors[:attachment].any?)
  end
end
