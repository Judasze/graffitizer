require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  def setup
    @message = FactoryGirl.create(:message)
  end

  test 'messages test' do
    assert_equal @message.recipient.recieved_messages, [@message]
    assert_equal @message.sender.sent_messages, [@message]
  end
end
