# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140728212233) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "battle_entries", force: true do |t|
    t.integer "battle_id"
    t.integer "user_id"
    t.integer "picture_id"
  end

  add_index "battle_entries", ["id"], name: "index_battle_entries_on_id", using: :btree
  add_index "battle_entries", ["user_id", "battle_id"], name: "index_battle_entries_on_user_id_and_battle_id", unique: true, using: :btree

  create_table "battle_votes", force: true do |t|
    t.integer "battle_entry_id"
    t.integer "user_id"
  end

  add_index "battle_votes", ["id"], name: "index_battle_votes_on_id", using: :btree
  add_index "battle_votes", ["user_id", "battle_entry_id"], name: "index_battle_votes_on_user_id_and_battle_entry_id", unique: true, using: :btree

  create_table "battles", force: true do |t|
    t.date    "beginning_date"
    t.date    "finish_date"
    t.text    "description"
    t.boolean "prize"
    t.text    "word"
  end

  add_index "battles", ["id"], name: "index_battles_on_id", using: :btree

  create_table "categories", force: true do |t|
    t.string "name"
  end

  create_table "chat_messages", force: true do |t|
    t.string "content",   null: false
    t.string "user_name", null: false
  end

  create_table "chats", force: true do |t|
    t.string  "message"
    t.integer "user_id"
  end

  add_index "chats", ["id"], name: "index_chats_on_id", using: :btree

  create_table "comments", force: true do |t|
    t.text    "content"
    t.integer "user_id"
    t.integer "throwup_id"
  end

  add_index "comments", ["throwup_id"], name: "index_comments_on_throwup_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "forem_categories", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
  end

  add_index "forem_categories", ["slug"], name: "index_forem_categories_on_slug", unique: true, using: :btree

  create_table "forem_forums", force: true do |t|
    t.string  "name"
    t.text    "description"
    t.integer "category_id"
    t.integer "views_count", default: 0
    t.string  "slug"
  end

  add_index "forem_forums", ["slug"], name: "index_forem_forums_on_slug", unique: true, using: :btree

  create_table "forem_groups", force: true do |t|
    t.string "name"
  end

  add_index "forem_groups", ["name"], name: "index_forem_groups_on_name", using: :btree

  create_table "forem_memberships", force: true do |t|
    t.integer "group_id"
    t.integer "member_id"
  end

  add_index "forem_memberships", ["group_id"], name: "index_forem_memberships_on_group_id", using: :btree

  create_table "forem_moderator_groups", force: true do |t|
    t.integer "forum_id"
    t.integer "group_id"
  end

  add_index "forem_moderator_groups", ["forum_id"], name: "index_forem_moderator_groups_on_forum_id", using: :btree

  create_table "forem_posts", force: true do |t|
    t.integer  "topic_id"
    t.text     "text"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "reply_to_id"
    t.string   "state",       default: "pending_review"
    t.boolean  "notified",    default: false
  end

  add_index "forem_posts", ["reply_to_id"], name: "index_forem_posts_on_reply_to_id", using: :btree
  add_index "forem_posts", ["state"], name: "index_forem_posts_on_state", using: :btree
  add_index "forem_posts", ["topic_id"], name: "index_forem_posts_on_topic_id", using: :btree
  add_index "forem_posts", ["user_id"], name: "index_forem_posts_on_user_id", using: :btree

  create_table "forem_subscriptions", force: true do |t|
    t.integer "subscriber_id"
    t.integer "topic_id"
  end

  create_table "forem_topics", force: true do |t|
    t.integer  "forum_id"
    t.integer  "user_id"
    t.string   "subject"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "locked",       default: false,            null: false
    t.boolean  "pinned",       default: false
    t.boolean  "hidden",       default: false
    t.datetime "last_post_at"
    t.string   "state",        default: "pending_review"
    t.integer  "views_count",  default: 0
    t.string   "slug"
  end

  add_index "forem_topics", ["forum_id"], name: "index_forem_topics_on_forum_id", using: :btree
  add_index "forem_topics", ["slug"], name: "index_forem_topics_on_slug", unique: true, using: :btree
  add_index "forem_topics", ["state"], name: "index_forem_topics_on_state", using: :btree
  add_index "forem_topics", ["user_id"], name: "index_forem_topics_on_user_id", using: :btree

  create_table "forem_views", force: true do |t|
    t.integer  "user_id"
    t.integer  "viewable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "count",             default: 0
    t.string   "viewable_type"
    t.datetime "current_viewed_at"
    t.datetime "past_viewed_at"
  end

  add_index "forem_views", ["updated_at"], name: "index_forem_views_on_updated_at", using: :btree
  add_index "forem_views", ["user_id"], name: "index_forem_views_on_user_id", using: :btree
  add_index "forem_views", ["viewable_id"], name: "index_forem_views_on_viewable_id", using: :btree

  create_table "likes", force: true do |t|
    t.integer "user_id"
    t.integer "throwup_id"
  end

  add_index "likes", ["throwup_id"], name: "index_likes_on_throwup_id", using: :btree
  add_index "likes", ["user_id", "throwup_id"], name: "index_likes_on_user_id_and_throwup_id", unique: true, using: :btree

  create_table "messages", force: true do |t|
    t.string   "subject"
    t.text     "content"
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["recipient_id"], name: "index_messages_on_recipient_id", using: :btree
  add_index "messages", ["sender_id"], name: "index_messages_on_sender_id", using: :btree

  create_table "throwups", force: true do |t|
    t.text     "content"
    t.string   "youtube"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.integer  "user_id"
    t.integer  "likes"
    t.integer  "dislikes"
    t.integer  "category_id",             default: 7, null: false
  end

  add_index "throwups", ["id"], name: "index_throwups_on_id", using: :btree
  add_index "throwups", ["user_id"], name: "index_throwups_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "role"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "crew"
    t.string   "city"
    t.string   "date"
    t.string   "email",                  default: "",               null: false
    t.string   "encrypted_password",     default: "",               null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,                null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "forem_admin",            default: false
    t.string   "forem_state",            default: "pending_review"
    t.boolean  "forem_auto_subscribe",   default: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["id"], name: "index_users_on_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "battle_entries", "battles", name: "battle_entries_battle_id_fk", dependent: :delete

  add_foreign_key "battle_votes", "battle_entries", name: "battle_votes_battle_entry_id_fk", dependent: :delete
  add_foreign_key "battle_votes", "users", name: "battle_votes_user_id_fk", dependent: :delete

  add_foreign_key "chats", "users", name: "chats_user_id_fk", dependent: :delete

  add_foreign_key "comments", "throwups", name: "comments_throwup_id_fk", dependent: :delete

  add_foreign_key "likes", "throwups", name: "likes_throwup_id_fk", dependent: :delete
  add_foreign_key "likes", "users", name: "likes_user_id_fk", dependent: :delete

  add_foreign_key "messages", "users", name: "messages_recipient_id_fk", column: "recipient_id", dependent: :delete
  add_foreign_key "messages", "users", name: "messages_sender_id_fk", column: "sender_id", dependent: :delete

end
