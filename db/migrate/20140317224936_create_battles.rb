class CreateBattles < ActiveRecord::Migration
  def change
    create_table :battles do |t|
      t.timestamp :beginning_date
      t.timestamp :finish_date
    end

    create_table :battle_entries do |t|
      t.integer :battle_id
      t.integer :user_id
      t.integer :picture_id
    end

    create_table :battle_vote do |t|
      t.integer :battle_entry_id
      t.integer :user_id
    end
  end
end
