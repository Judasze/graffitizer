class AddLikesAndDislikesToThrowups < ActiveRecord::Migration
  def change
    add_column :throwups, :likes, :integer
    add_column :throwups, :dislikes, :integer
  end
end
