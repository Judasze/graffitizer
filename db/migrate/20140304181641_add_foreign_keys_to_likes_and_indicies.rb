class AddForeignKeysToLikesAndIndicies < ActiveRecord::Migration
  def up
    add_index :likes, [:user_id, :throwup_id], :unique => true
    add_index :likes, :throwup_id
    add_index :messages, :sender_id
    add_index :messages, :recipient_id
    add_index :users, :id
    add_index :throwups, :id

    add_foreign_key(:likes, :throwups, dependent: :delete)
    add_foreign_key(:likes, :users, dependent: :delete)
  end

  def down

    remove_index :likes, [:user_id, :throwup_id], :unique => true
    remove_index :likes, :throwup_id
    remove_index :messages, :sender_id
    remove_index :messages, :recipient_id
    remove_index :users, :id
    remove_index :throwups, :id

    remove_foreign_key(:likes, :throwups, dependent: :delete)
    remove_foreign_key(:likes, :users, dependent: :delete)
  end
end
