class AddMissingForeignKeysAndIndicies < ActiveRecord::Migration
  def change
    add_index :battle_entries, [:user_id, :battle_id], :unique => true
    add_index :battle_votes, [:user_id, :battle_entry_id], :unique => true
    add_index :battles, :id
    add_index :battle_entries, :id
    add_index :battle_votes,:id

    add_foreign_key(:battle_votes, :users, dependent: :delete)
    add_foreign_key(:battle_votes, :battle_entries, dependent: :delete)
    add_foreign_key(:battle_entries, :battles, dependent: :delete)
  end
end
