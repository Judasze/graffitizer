class ChangeDateTypeInBattlesDates < ActiveRecord::Migration
  def change
    change_column :battles, :beginning_date, :date
    change_column :battles, :finish_date, :date
  end
end
