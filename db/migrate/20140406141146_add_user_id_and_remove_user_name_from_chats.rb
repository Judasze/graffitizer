class AddUserIdAndRemoveUserNameFromChats < ActiveRecord::Migration
  def change
    remove_column :chats, :user_name
    add_column :chats, :user_id, :integer
  end
end
