class AddUserRefToThrowups < ActiveRecord::Migration
  def change
    add_reference :throwups, :user, index: true
  end
end
