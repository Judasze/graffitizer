class AddDescriptionAndPrizeToBattle < ActiveRecord::Migration
  def change
    add_column :battles, :description, :text
    add_column :battles, :prize, :boolean
  end
end
