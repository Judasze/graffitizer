class CreateChat < ActiveRecord::Migration
  def change
    create_table :chats do |t|
      t.string :message
      t.string :user_name
    end
  end
end
