class AddAttachmentAttachmentToThrowups < ActiveRecord::Migration
  def self.up
    change_table :throwups do |t|
      t.attachment :attachment
    end
  end

  def self.down
    drop_attached_file :throwups, :attachment
  end
end
