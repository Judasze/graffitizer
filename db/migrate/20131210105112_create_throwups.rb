class CreateThrowups < ActiveRecord::Migration
  def change
    create_table :throwups do |t|
      t.text :content
      t.string :youtube
    end
  end
end
