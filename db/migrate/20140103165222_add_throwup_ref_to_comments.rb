class AddThrowupRefToComments < ActiveRecord::Migration
  def change
    add_reference :comments, :throwup, index: true
  end
end
