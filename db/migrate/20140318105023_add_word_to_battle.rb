class AddWordToBattle < ActiveRecord::Migration
  def change
    add_column :battles, :word, :text
  end
end
