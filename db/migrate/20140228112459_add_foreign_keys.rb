class AddForeignKeys < ActiveRecord::Migration
  def change
    add_foreign_key(:comments, :throwups, dependent: :delete)
    add_foreign_key(:messages, :users, column: 'sender_id', dependent: :delete)
    add_foreign_key(:messages, :users, column: 'recipient_id', dependent: :delete)
  end
end
