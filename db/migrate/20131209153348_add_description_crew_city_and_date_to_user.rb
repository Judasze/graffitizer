class AddDescriptionCrewCityAndDateToUser < ActiveRecord::Migration
  def change
    add_column :users, :crew, :string
    add_column :users, :city, :string
    add_column :users, :date, :string
  end
end
