class CreateCategoryAndaddCategoryIdtoThrowup < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
    end

    add_column :throwups, :category_id, :integer, null: false, default: 7

  end
end
