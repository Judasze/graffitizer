class RenameBattleVoteToBattleVotes < ActiveRecord::Migration
  def change
    rename_table :battle_vote, :battle_votes
  end
end
