class AddForeignKeyAndIndexToChats < ActiveRecord::Migration
  def change
    add_foreign_key( :chats, :users, dependent: :delete)
    add_index :chats, :id
  end
end
