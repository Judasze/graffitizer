class PresentUserController < ApplicationController
  def show
    if user_signed_in?
      @user = current_user
    else
      redirect_to new_user_session_path
    end
  end

  def update
    @user = current_user
    @user.update_attributes!(user_params)
    redirect_to show
  end

  private

    def user_params
      params.require(:user).permit(:name, :description, :city, :crew, :date)
    end
end
