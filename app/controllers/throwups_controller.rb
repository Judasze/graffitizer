class ThrowupsController < ApplicationController
  before_filter :authenticate_user!,
    :only => [:create, :edit]

  def new
    @throwup = Throwup.new
  end

  def index
    @throwups = Throwup.paginate :page => params[:page], :per_page => 12, :order => "id DESC"
    @legals = Throwup.legals.paginate :page => params[:pge], :per_page => 12, :order => "id DESC"
    @illegals = Throwup.illegals.paginate :page => params[:pge], :per_page => 12, :order => "id DESC"
    @trains = Throwup.trains.paginate :page => params[:pge], :per_page => 12, :order => "id DESC"
    @sketches = Throwup.sketches.paginate :page => params[:pge], :per_page => 12, :order => "id DESC"
    @stencils = Throwup.stencils.paginate :page => params[:pge], :per_page => 12, :order => "id DESC"
    @tags = Throwup.tags.paginate :page => params[:pge], :per_page => 12, :order => "id DESC"
    @various = Throwup.various.paginate :page => params[:pge], :per_page => 12, :order => "id DESC"

  end

  def create
    @throwup = current_user.throwups.new(throwup_params)
    @throwup.save
    if @throwup.save
      redirect_to throwups_path
    else
      render acttion: :new
    end
  end

  def show
    @throwup = Throwup.find(params[:id])
    @counter = @throwup.likes.count
    @comment = Comment.new
    @next_throwup = Throwup.where("id >?", @throwup.id).first
    @prev_throwup = Throwup.where("id <?", @throwup.id).last
    if current_user.present?
      @like = current_user.likes.build(throwup_id: @throwup.id)
    else
      nil
    end
  end

  private

    def throwup_params
      params.require(:throwup).permit(:content, :youtube, :city, :crew, :date, :attachment, :category_id)
    end
end
