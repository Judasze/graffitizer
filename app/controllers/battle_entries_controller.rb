class BattleEntriesController < ApplicationController
  before_filter :authenticate_user!,
    :only => [:new, :create]

  def new
    battle = Battle.find(params[:battle_id])
    @battle_entry = battle.battle_entries.new user_id: current_user.id
  end

  def create
    battle = Battle.find(params[:battle_id])
    throwup = current_user.throwups.new(throwup_params)
    throwup.save
    @battle_entry = battle.battle_entries.new user_id: current_user.id, picture_id: throwup.id
    @battle_entry.save
    redirect_to battle_path(battle)
  end

  private

    def throwup_params
      params.require(:battle_entry).require(:throwup).permit(:attachment, :content)
    end
end
