class MessagesController < ApplicationController

  before_filter :authenticate_user!

  def index
    @user = current_user
    @received = @user.recieved_messages
    @sent = @user.sent_messages
  end

  def new
    @message = Message.new
    @recipient = User.find(params[:recipient_id])
  end

  def create
    @message = Message.new(message_params)
    @message.save
    if @message.save
      MessageMailer.new_message(User.find(message_params[:recipient_id])).deliver
      flash[:notice] = "Wysłano wiadomość"
      redirect_to messages_path
    else
      render action: :new
    end
  end

  private

  def message_params
    params.require(:message).permit(:recipient_id, :sender_id, :subject, :content)
  end
end
