module Users
  class ThrowupsController < ApplicationController

    def index
      @user = User.find(params[:user_id])
      @throwups = @user.throwups.paginate :page => params[:page], 
        :per_page => 12, :order => "id DESC"

    end

  end
end
