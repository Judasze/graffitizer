class BattlesController < ApplicationController
  before_filter :authenticate_user!,
    :only => [:new, :show, :create]
  def new
    @battle = Battle.new
  end

  def create
    @battle = Battle.new(battle_params)
    @battle.save!
    redirect_to battles_path
  end

  def index
    @finished_battles = Battle.finished
    @active_battles = Battle.active
  end

  def show
    @battle = Battle.find(params[:id])
    @active_battles = Battle.active

    if @battle.battle_entries.any?
      @winners = @battle.battle_entries.group_by{|c| c.battle_votes.count}.sort_by{|k, v| -k}
      .first.last.map do |w| User.find(w.user_id) end
    else
      @winners = []
    end
  end

  private

  def battle_params
    params.require(:battle).permit(:word, :prize, :description, :beginning_date, :finish_date)
  end
end



