class CommentsController < ApplicationController
  before_filter :authenticate_user!

  def new
    @comment = Comment.new
    @throwup = Throwup.find(params[:throwup_id])
  end

  def create
    @comment = current_user.comments.new(comment_params)
    @comment.save
    render_comments_index
  end


  private

  def throwup
    @throwup ||= Throwup.find(params[:throwup_id])
  end

  def render_comments_index
    render partial: 'index',
      locals: { throwup: throwup }
  end

  def comment_params
    comment = params.require(:comment).permit(:content)
    comment.merge(throwup_id: params[:throwup_id])
  end
end
