class ChatsController < ApplicationController 
  before_filter :authenticate_user!,
    :only => [:index, :create]

  def index
    @chats = Chat.all
    gflash :warning => 'Wiadomości chatu nie są zapisywane na serwerze'
  end

  def create
    @chat = Chat.create!(chat_params)
  end

  private

  def chat_params
    params.require(:chat).permit(:message, :user_id)
  end
end
