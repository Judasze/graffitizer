class LikesController < ApplicationController
  layout false

  def create
    current_user.likes.create! throwup: throwup

    render_like_section
  end

  def destroy
    current_user.likes.find_by_throwup_id(throwup.id).destroy!

    render_like_section
  end

  private

  def throwup
    @throwup ||= Throwup.find(params[:throwup_id])
  end

  def counter
    throwup.likes.count
  end

  def render_like_section
    render partial: 'like',
      locals: { counter: counter, throwup: throwup }
  end
end
