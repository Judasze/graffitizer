class BattleVotesController < ApplicationController
  before_filter :authenticate_user!,
    :only => [:create, :destroy]

  layout false

  def create
    @active_battles = Battle.active
    current_user.battle_votes.create! battle_entry: battle_entry
    render_battle_votes_section 
  end

  def destroy
    @active_battles = Battle.active
    current_user.battle_votes.find_by_battle_entry_id(battle_entry.id).destroy!
    render_battle_votes_section
  end

  private

    def battle_entry
      battle_entry = BattleEntry.find(params[:battle_entry_id])
    end

    def counter
      battle_entry.battle_votes.count
    end

    def battle
      Battle.find(battle_entry.battle_id)
    end

    def render_battle_votes_section
      render partial: 'battle_entries/index',
        locals: { battle: battle, counter: counter, battle_entry: battle_entry, }
    end
end
