$(function() {
  $(document).on("ajax:success", "#battle_votes_section", function(e, data, status, xhr) {
    $('.battle-contenders').replaceWith(data);
  });
});
