class BattleEntry < ActiveRecord::Base

  # validation below does not work.
  #validates_associated :throwup

  validate do
    [:picture_id].each do |attr|
      errors.add(attr, 'Nie można uczestniczyć w battlu nie dodając pracy!') if self.send(attr).nil?
    end
  end

  belongs_to :battle
  belongs_to :user
  has_one :throwup
  has_many :battle_votes, dependent: :destroy

  accepts_nested_attributes_for :throwup

end
