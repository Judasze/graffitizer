class BattleVote < ActiveRecord::Base
  belongs_to :user
  belongs_to :battle_entry
end
