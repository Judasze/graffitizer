class Message < ActiveRecord::Base
  validates :subject, presence: true
  validates :content, presence: true

  belongs_to :sender, :class_name => 'User'
  belongs_to :recipient, :class_name => 'User'
end
