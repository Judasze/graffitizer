class Comment < ActiveRecord::Base
  validates :throwup, presence: true
  validates :user, presence: true
  validates :content, presence: true

  belongs_to :throwup
  belongs_to :user
end
