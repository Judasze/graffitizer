class Throwup < ActiveRecord::Base
  has_attached_file :attachment, styles: {
    thumb: '150x150>',
    large: '500x500>'
  }

  validates_attachment_content_type :attachment, :content_type => ["image/jpg", "image/jpeg", "image/png"]
  validates :attachment, presence: true, if: :youtube_nil?
  validates :youtube, presence: true, if: :attachment_nil?

  belongs_to :user
  belongs_to :battle_entry
  belongs_to :category
  has_many :comments, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :liking_users, through: :likes

  scope :legals, -> { where(:category_id => '1') }
  scope :illegals, -> { where(:category_id => '2') }
  scope :trains, -> { where(:category_id => '3') }
  scope :sketches, -> { where(:category_id => '4') }
  scope :stencils, -> { where(:category_id => '5') }
  scope :tags, -> { where(:category_id => '6') }
  scope :various, -> { where(:category_id => '7') }




  def attachment_nil?
    !attachment?
  end

  def youtube_nil?
    youtube.nil?
  end

  def youtube_id
    if youtube[/youtu\.be\/([^\?]*)/]
      $1
    else
      youtube[/^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/]
      $5
    end
  end
end
