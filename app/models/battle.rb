class Battle < ActiveRecord::Base

  scope :finished, -> { where('finish_date < ?', Date.today) }
  scope :active, -> { where('finish_date > ? AND beginning_date <= ?', Date.today, Date.today) }

  has_many :battle_entries, dependent: :destroy
end
