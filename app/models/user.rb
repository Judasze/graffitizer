class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :confirmable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true, uniqueness: true

  has_many :comments, dependent: :destroy
  has_many :liked_throwups, through: :likes
  has_many :likes, dependent: :destroy
  has_many :recieved_messages, class_name: "Message", foreign_key: "recipient_id"
  has_many :sent_messages, class_name: "Message", foreign_key: "sender_id"
  has_many :throwups, dependent: :destroy
  has_many :battle_entries, dependent: :destroy
  has_many :battle_votes, dependent: :destroy
  has_many :chats, dependent: :destroy

end
