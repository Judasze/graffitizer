module ThrowupHelper
  def youtube_thumbnail(youtube_id)
    "http://img.youtube.com/vi/#{youtube_id}/1.jpg"
  end
  def youtube_player(youtube_id)
    %Q{<iframe title="YouTube video player" width="640" height="390" src="http://www.youtube.com/embed/#{ youtube_id }" frameborder="0" allowfullscreen></iframe>}.html_safe
    #%iframe{ :width => 640, :height => 390, :frameborder => "0", :src => "//www.youtube.com/embed/#{youtube_id}" }
  end
end
