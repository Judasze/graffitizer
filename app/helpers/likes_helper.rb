module LikesHelper
  def like_button(throwup)
    liked = current_user.likes.find_by_throwup_id(throwup.id).present?

    link_to (liked ?  'Unprops!' : 'Props!'), throwup_like_path(throwup),
      class: "btn btn-mini", method: (liked ? :delete : :post), remote: true
  end
end
