module BattleHelper
  def vote_button(battle_entry)
    voted = current_user.battle_votes.find_by_battle_entry_id(battle_entry.id).present?

    link_to (voted ? 'Unprops!' : 'Props!'), battle_entry_battle_vote_path(battle_entry, battle_id: @battle),
      class: "btn btn-mini", method: (voted ? :delete : :post), remote: true
  end
end
