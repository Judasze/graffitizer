class MessageMailer < ActionMailer::Base
  default from: "Graffitizer@graffitizer.com"

  def new_message(user)
    @user = user
    @url = 'http://secret-retreat-5703.herokuapp.com/messages'
    mail(to: @user.email, subject: 'Dostałeś nową wiadomość')
  end
end
